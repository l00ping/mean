const express = require('express');
const router = express.Router();
const BlogPost = require('../../models/Blogpost');
const mongoose = require('mongoose');
const multer = require('multer');
const crypto = require('crypto');
const path = require('path');

router.get('/ping', (req, res) => {
    res.status(200).json({msg: 'pong', date: new Date()})
});

let lastUploadedImageName = '';

// @route   POST api/posts
// @desc    Create post
// @access  Public
router.post('/', (req, res) => {
    const blogPost = new BlogPost({...req.body, image: lastUploadedImageName});
    blogPost.save()
        .then(blogposts => {
            res.status(200).json(blogposts)
        })
        .catch(err => res.status(400).json(err));
});


// File upload configuration
let storage = multer.diskStorage({
    destination:  './uploads/',
    filename: function(req, file, callback) {
        crypto.randomBytes(16, function (err, raw) {
            if (err) return callback(err);
//            callback(null, raw.toString('hex') + path.extname(file.originalname));
            lastUploadedImageName = raw.toString('hex') + path.extname(file.originalname);
            callback(null, lastUploadedImageName);
        });
    }
});
const upload = multer({storage});

// file upload
router.post('/images', upload.single("image"), function (req,res) {
    if(!req.file.originalname.match(/\.(jpg|jpeg|bmp|png)$/)) {
        return res.status(400).json({msg: 'only image files please'});
    }
    res.status(200).send({fileName: req.file.filename, file: req.file})
});

// @route   GET api/posts
// @desc    Get post
// @access  Public
router.get('/', (req, res) => {
    BlogPost.find()
        .sort({'createdOn': -1})
        .then(posts => res.json(posts))
        .catch(err => res.status(404).json({nopostsfound: 'No posts found'}));
});

// @route   GET api/posts/:id
// @desc    Get post by id
// @access  Public
router.get('/:id', (req, res) => {
    BlogPost.findById(req.params.id)
        .then(post => res.json(post))
        .catch(err => res.status(404).json({nopostfound: 'No post found with that id'}));
});


// @route   DELETE api/posts/:id
// @desc    Delete post by id
// @access  Public
router.delete('/:id', (req, res) => {
    BlogPost.findByIdAndDelete(req.params.id)
        .then(post => res.status(200).json({post: 'Post Deleted'}))
        .catch(err => res.status(404).json({nopostfound: 'No post found with that id'}));
});

// @route   DELETE api/posts/?ids=ids1,ids2,ids3..
// @desc    Delete multiple post by ids
// @access  Public
router.delete('/', (req, res) => {
    const ids = req.query.ids;
    const allIds = ids.split(',').map(id => {
        if (id.match(/^[0-9a-fA-F]{24}$/)) {
            return mongoose.Types.ObjectId((id))
        } else {
            console.log('id is not valid', id);
        }
    });
    const condition = {_id: {$in: allIds}};
    BlogPost.deleteMany(condition)
        .then(post => res.status(200).json({post: 'Posts Deleted'}))
        .catch(err => res.status(404).json({nopostfound: 'No posts found with these id'}));
});


module.exports = router;