const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');

//const passport = require('passport');

const posts = require('./routes/v1/posts');
//const profile = require('./routes/api/profile');
//const posts = require('./routes/api/posts');

const app = express();

app.use(cors());
// Body parser middleware
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());


const uploadDir = require('path').join(__dirname, '/uploads');
app.use(express.static(uploadDir));

// DB Config
const db = require('./config/keys').mongoURI;

// Connect to MongoDB
mongoose
    .connect(db, { useNewUrlParser: true })
    .then(() => console.log('MongoDB connected'))
    .catch(() => console.log(err));

// Passport middleware
//app.use(passport.initialize());

// Passport Config
//require('./config/passport')(passport);

// Use Routes
 app.use('/api/v1', posts);
// app.use('/api/profile', profile);
// app.use('/api/posts', posts);

const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`Server running on port ${port}`));