import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http"
import {environment} from '../../environments/environment';
import {Observable, Subject} from "rxjs/index";
import {Blogpost} from "../models/blogpost";

@Injectable({
  providedIn: 'root'
})

export class BlogpostService {
  private url = environment.url;
  private blogPostCreated = new Subject<string>();

  constructor(private httpClient: HttpClient) {
  }

  dispatchBlogPostCreated(id: string) {
    this.blogPostCreated.next(id);
  }

  handleBlogPostCreated() {
    return this.blogPostCreated.asObservable();
  }

  uploadImageBlogPost(formData: FormData) {
    return this.httpClient.post<any>(`${this.url}`+'/images', formData)
  }

  createBlogPost(blogpost: Blogpost) {
    return this.httpClient.post<Blogpost>(`${this.url}`, blogpost);
  }

  getBlogPosts(): Observable<Blogpost[]> {
    return this.httpClient.get<Blogpost[]>(`${this.url}`);
  }

  getBlogPostById(id): Observable<Blogpost> {
    return this.httpClient.get<Blogpost>(`${this.url}` + id);
  }

  deleteSingleBlogPost(id: string) {
    return this.httpClient.delete<Blogpost>(`${this.url}` + id);
  }

  deleteBlogPosts(ids: string[]) {
    const allIds = ids.join(','); // 'ids1, ids2, ids3'
    return this.httpClient.delete<Blogpost>(`${this.url}` +'/?ids=' + allIds);
  }

}
