import {Component, OnInit} from '@angular/core';
import {Blogpost} from "../../models/blogpost";
import {Observable} from "rxjs/index";
import {BlogpostService} from "../../api/blogpost.service";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-blogpost-list',
  templateUrl: './blogpost-list.component.html',
  styleUrls: ['./blogpost-list.component.scss']
})
export class BlogpostListComponent implements OnInit {
  blogPostList$: Observable<Blogpost[]>;
  private imagePath = environment.imagePath;

  constructor(private blogpostService: BlogpostService) {
  }

  ngOnInit() {
    this.blogPostList$ = this.blogpostService.getBlogPosts();
  }

}
