import {Component, OnInit} from '@angular/core';
import {Blogpost} from "../../models/blogpost";
import {BlogpostService} from "../../api/blogpost.service";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  allBlogPosts: Blogpost[];

  constructor(private blogpostService: BlogpostService) {
  }

  ngOnInit() {
    this.refresh();

    this.blogpostService.handleBlogPostCreated()
      .subscribe(
        data => this.refresh(),
        errors => this.handleError(errors)
      )
  }

  deleteBlogPosts(selectedOptions) {
    const ids = selectedOptions.map(so => so.value);
    if (ids.length === 1) {
      this.blogpostService.deleteSingleBlogPost(ids[0])
        .subscribe(
          data => this.refresh(),
          err => this.handleError(err))
    } else {
      this.blogpostService.deleteBlogPosts(ids)
        .subscribe(
          data => this.refresh(),
          err => this.handleError(err))
    }
  }

  refresh() {
    this.blogpostService.getBlogPosts()
      .subscribe(
        data => this.allBlogPosts = data,
        error => this.handleError(error))
  }

  handleError(error) {
    console.log(error);
  }

}
