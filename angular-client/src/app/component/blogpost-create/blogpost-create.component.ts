import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, FormGroupDirective} from "@angular/forms";
import {BlogpostService} from "../../api/blogpost.service";

@Component({
  selector: 'app-blogpost-create',
  templateUrl: './blogpost-create.component.html',
  styleUrls: ['./blogpost-create.component.scss']
})
export class BlogpostCreateComponent implements OnInit {
  creationForm: FormGroup;
  @ViewChild('image') image: ElementRef;

  constructor(private fb: FormBuilder,
              private blogPostService: BlogpostService,
              private el: ElementRef) {
  }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.creationForm = this.fb.group({
      title: '',
      subtitle: '',
      content: '',
      image: ''
    })
  }

  upload() {
    // retrieve file upload by HTML tag
    let fileCount: number = this.image.nativeElement.length;
    let formData = new FormData();
    formData.append('image', this.image.nativeElement.files[0]);
    this.blogPostService.uploadImageBlogPost(formData)
      .subscribe(
        data => console.log(data),
        err => console.log(err)
      )
  }

  createBlogPost(formDirective: FormGroupDirective) {
    if (this.creationForm.valid) {
      let fileCount: number = this.image.nativeElement.length;
      let formData = new FormData();
      formData.append('image', this.image.nativeElement.files[0]);
      this.blogPostService.uploadImageBlogPost(formData)
        .subscribe(
          data => {
            this.blogPostService.createBlogPost(this.creationForm.value).subscribe(
              data => this.handleSucces(data, formDirective),
              err => this.handleError(err)
            )
          },
          err => console.log(err)
        );
    }
  }

  handleSucces(data, formDirective) {
    this.creationForm.reset();
    formDirective.resetForm();
    this.blogPostService.dispatchBlogPostCreated(data._id);
  }

  handleError(err) {
    console.log('Err: Blog post not created', err);
  }


}
