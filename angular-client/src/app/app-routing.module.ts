import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BlogpostListComponent} from "./layout/blogpost-list/blogpost-list.component";
import {BlogpostComponent} from "./layout/blogpost/blogpost.component";
import {ErrorPageComponent} from "./layout/error-page/error-page.component";
import {AdminComponent} from "./layout/admin/admin.component";

const routes: Routes = [
  {path: 'blog', component: BlogpostListComponent},
  {path: 'blog/:id', component: BlogpostComponent},
  {path: 'admin', component: AdminComponent},
  {path: '**', component: ErrorPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
