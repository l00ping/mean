import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule, FormsModule} from "@angular/forms";

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BlogpostComponent} from './layout/blogpost/blogpost.component';
import {BlogpostListComponent} from './layout/blogpost-list/blogpost-list.component';
import {BlogpostService} from "./api/blogpost.service";
import {HttpClientModule} from "@angular/common/http";
import {MaterialModule} from "./material.module";
import { ErrorPageComponent } from './layout/error-page/error-page.component';
import { AdminComponent } from './layout/admin/admin.component';
import { BlogpostCreateComponent } from './component/blogpost-create/blogpost-create.component';

@NgModule({
  declarations: [
    AppComponent,
    BlogpostComponent,
    BlogpostListComponent,
    ErrorPageComponent,
    AdminComponent,
    BlogpostCreateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [BlogpostService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
