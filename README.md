# README #



http://pierrelouisrebours.com

# How to run back and frontend in the same time #
It is very simple. 

Go on the node-server folder and write in your terminal :

                npm run install install-client
                
                npm run install install-server

And when it is finished

                npm run dev


Backend: http://localhost:3000

Frontend: http://localhost:4200


# /!\ PROJECT IN PROGRESS /!\ #

creating a JavaScript FullStack application

- create a REST API using Node and Express.js

- CRUD (Create Read Update Delete) to a MongoDB database using Mongoose

- authenticate users with Passport

- create an Angular client (using Angular 7)

- make AJAX requests from Angular client to my REST API

- improve the "look & feel" of my Angular applications with Angular Material

- post content from an Angular form to my Express server

- upload images to my Express server

- generate images of reduced size automatically from the uploaded images

- etc ...